"Pibitron"

A Rasperry Pi and sensor project using Python.

Tracked Motion, Light Level, Temperature, Humidity, and Audio Level.
Data was uploaded to Azure Table Storage via an Event Hub and Stream Analytics.

The case was made using a template for the Raspberry Pi, modified by me using Blender, and then 3D printed.
Additionally, I designed the motion sensor section from scratch.

