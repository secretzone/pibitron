#! /usr/bin/env python
from __future__ import print_function
# Python program to communicate with an MCP3008

#Import our SpiDev wrapper and our sleep function
import spidev
import socket
import RPi.GPIO as GPIO
import sys
import Adafruit_DHT
import thread
import time
import numpy
import pyaudio
import analyse
import wave
from proton import Message, Messenger
from proton.handlers import MessagingHandler
from proton.reactor import Container
#from azure.servicebus import ServiceBusService, Message, Queue
##import alsaaudio, audioop
from time import sleep
print ("Imported libraries")
GPIO.setmode(GPIO.BCM)
print ("Set GPIO mode")
global cur_light, cur_sound, cur_motion, cur_temp, cur_hum, done
led_green = 27
sensor_light = 1
motion = 18
done = 1
cur_temp = '\"Temperature\":0.0'
cur_sound = '\"Sound\":0'
cur_motion = '\"Motion\":0'
cur_hum = '\"Humidity\":0.0'
cur_light = '\"Light\":0'
print ("Set global defaults")

GPIO.setup(motion, GPIO.IN) #Motion
print ("Set up motion")

GPIO.setup(led_green, GPIO.OUT) #Green LED
print ("Set up led_green")

print ('Establishing SPI Device on Bus 0, Device 0')
spi = spidev.SpiDev()
spi.open(0,0)
c =[0,0,0,0,0,0,0,0]
i=0

for i in range(0,8):
    c[i] = open('channel{0:1d}'.format(i), 'w')
    print (c[i])

print ("Set up SPI")

def getAdc(channel, name):
    if((channel > 7) or (channel < 0)):
        return '\"Unknown\":0'
    r = spi.xfer([1, (8+channel) << 4, 0])
    adcOut = (((r[1]&3) << 8) + r[2])
    percent = int(round(adcOut/10.24))
    rt = ('\"{0}\":{1:4d}'.format(name, adcOut))
    sleep(0.1)
    return rt
    
def getGPIO(pin, name):
    #print (pin)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin, GPIO.IN) #Motion
    GPIO.input(pin)
    gp = open('GPIO{0:1d}'.format(pin), 'w')
    val = GPIO.input(pin)
    rt = ("\"{0}\":{1:1d}".format(name,val))
    return rt
    
def getDHT():
    done = 0
    humidity, temperature = Adafruit_DHT.read_retry(22, 23)
    global cur_temp, cur_hum
    if humidity is not None and temperature is not None:
        cur_temp = '\"Temperature\":{0:0.1f}'.format(temperature)
        cur_hum = '\"Humidity\":{0:0.1f}'.format(humidity)
    else:
        cur_temp = '\"Temperature\":0.0'
        cur_hum = '\"Humidity\":0.0'
    done = 1


def getAudio():
    #return "\"Sound\":0"
    # Initialize PyAudio

    pyaud = pyaudio.PyAudio()
    # Open input stream, 16-bit mono at 44100 Hz
    # On my system, device 2 is a USB microphone, your number may differ.
    stream = pyaud.open(
    format = pyaudio.paInt16,
    channels = 1,
    rate = 48000,
    input_device_index = 0,
    input = True,
    frames_per_buffer = 8192)   

    

    #while True:
    # Read raw microphone data
    rawsamps = stream.read(1024)
    # Convert raw data to NumPy array
    samps = numpy.fromstring(rawsamps, dtype=numpy.int16)
    # Show the volume and pitch
    #print analyse.loudness(samps), analyse.musical_detect_pitch(samps)
    vol = analyse.loudness(samps)
    stream.stop_stream()
    stream.close()
    pyaud.terminate()
    rt = "\"Sound\":{0}".format(vol)

    return rt



def SendMessage(s):
    messenger = Messenger()
    message = Message()
    User = 'Sender'

    Key = 'REMOVED'
    Namespace = 'REMOVED'
    Entity = 'pibitron'
    message.address = 'sb://{0}.servicebus.windows.net/publisher/200/messages?sync-publish=false;SharedAccessKeyName={1};SharedAccessKey={2};TransportType=Amqp'.format(Namespace, User, Key)

    print(message.address)
    message_temp = u'{0}'.format(s)
    message.body = message_temp.encode('utf-8')
    print (message.body)
    messenger.put(message)
    messenger.send()


def GetString(light,sound,motion,temp,hum):
    host = socket.gethostname()
    msg = '\"DeviceId\":\"{0}\",{1},{2},{3},{4},{5}'.format(host,light, sound, motion, temp, hum)
    msg = '{' + msg + '}'
    msg = msg.encode("utf-8")
    #print(msg)
    return b'{0}'.format(msg)






print ("Starting loop")

try:

    
    messenger = Messenger()
    message = Message()
    message.address = "amqps://Sender:'REMOVED'@innovationlabns.servicebus.windows.net/pibitron"
    while True:
        msg = ""
        cur_light = getAdc(sensor_light, "Light")
        cur_motion = getGPIO(motion, "Motion")
        cur_sound = getAudio()
        if (done == 1):
            thread.start_new_thread(getDHT, ())
        msg = GetString(cur_light,cur_sound,cur_motion,cur_temp, cur_hum)
        #SendMessage(msg)
        #info = Message(msg)
        #bus_service.send_queue_message('taskqueue', info)
        #print(msg)
        message.body = msg
        print(message.body)
        messenger.put(message)
        messenger.send()
        sleep(1)
finally:
    print ("Cleaning up")
    GPIO.cleanup()
    #stream.stop_stream()
    #stream.close()
    #pyaud.terminate()
